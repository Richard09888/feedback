from config import db
import os, sys
lib_path = os.path.abspath(os.path.join('..'))
sys.path.append(lib_path)


class Feedback(db.Model):
    __tablename__ = 'feedback'
    feedback_id = db.Column(db.String(200), primary_key=True)
    user_email = db.Column(db.String(200), nullable=False)
    _type = db.Column(db.String(50), nullable=False)
    level = db.Column(db.String(50), nullable=False)
    message = db.Column(db.Text, nullable=False)
    timestamp = db.Column(db.DateTime(), nullable=False)


def __init_feedback_db():
    db.create_all()
    db.session.commit()
    print("finish the database - feedback database!")
