from config import application, db


@application.teardown_request
def teardown_request(exception):
    if exception:
        application.logger.error(exception)
    try:
        db.session.commit()
    except:
        db.session.rollback()
    finally:
        db.session.close()
        db.session.remove()


if __name__ == '__main__':
    # for testing
    application.run(host=application.config['HOST'], port=application.config['PORT'], debug=application.config['DEBUG'])

    # for deployment
    # app = application
    # app.run()
