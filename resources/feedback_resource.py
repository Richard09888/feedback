import os
import sys
lib_path = os.path.abspath(os.path.join('..'))
sys.path.append(lib_path)

from datetime import datetime
import hashlib

from flask_restful import Resource
from flask import request, jsonify, json
from config import feedback_logger, db, api
from model.model import Feedback
from authorize.authorize import authorize


class FeedbackResource(Resource):
    @staticmethod
    def post():
        return_resp = {
            'feedback_id': '',
            'user_email': '',
            'type': '',
            'level': '',
            'message': '',
            'request_result': 'rejected',
            'error_message': ''
        }

        try:
            request_data = request.get_json(force=True)
        except Exception as ex:
            feedback_logger.warning('Unable to read object from backend. Expecting JSON.' + str(ex))
            return_resp['error_message'] = 'Unable to read Data. Expecting Json.'
            return jsonify(return_resp)

        # load into json if received a string
        try:
            if isinstance(request_data, str):
                post_data = json.loads(request_data)
                request_data = post_data
        except Exception as ex:
            feedback_logger.error('Object could not be parsed. ' + str(ex))
            return_resp['error_message'] = 'Unable to read Data.'
            return jsonify(return_resp)

        try:
            credentials = request_data.get('credentials')
            if not credentials:
                feedback_logger.info('request does not have credentials. ' + json.dumps(request_data))
                return jsonify({'message': 'credentials not found in request.', 'status': '0'})

            email = credentials.get('email')
            token = credentials.get('token')
            service_id = credentials.get('service_id')
            authorized = authorize(email, token, service_id)

            if authorized != '1':
                feedback_logger.info('User ' + str(email) + ' is not authorized.')
                return jsonify({'message': 'Unable to authorize this user', 'status': '0'})
        except Exception as ex:
            feedback_logger.exception('Exception authenticating user for request [' + json.dumps(request_data) + ']: ' + str(ex))
            return jsonify({'message': 'Failed to authorize this user', 'status': '0'})
        
        try:
            data = request_data.get('data')
            return_resp['user_email'] = email

            _type = data['_type']
            type_list = ['Question', 'Feedback', 'Criticism', 'Bug']
            if not _type or _type not in type_list:
                _type = 'Question'
            return_resp['type'] = _type

            level = data['level']
            level_types = ['critical', 'high', 'medium', 'low']
            if not level or level not in level_types:
                level = 'medium'
            return_resp['level'] = level

            message = data['message']
            if not message:
                return_resp['error_message'] = 'user email cannot be empty'
                return jsonify(return_resp)
            return_resp['message'] = message
        except Exception as e:
            feedback_logger.exception(str(e))
            return_resp['error_message'] = 'Could not get required params'
            return jsonify(return_resp)
        
        # write request data to db
        try:
            # generate feedback id
            submission_date = datetime.now()
            feedback_id = FeedbackResource.generate_job_id(submission_date)

            feedback = Feedback()
            feedback.feedback_id = feedback_id
            feedback.user_email = email
            feedback._type = _type
            feedback.level = level
            feedback.message = message
            feedback.timestamp = datetime.now()

            db.session.add(feedback)
            db.session.commit()
        except Exception as ex:
            db.session.rollback()
            db.session.remove()
            feedback_logger.exception('Error: '+str(ex))
            return_resp['error_message'] = 'Invalid Request.'
            return jsonify(return_resp)
        
        # return job_id
        return jsonify({
            'feedback_id': feedback_id,
            'user_email': email,
            'type': _type,
            'level': level,
            'timestamp': feedback.timestamp,
            'message': message,
            'request_result': 'accepted',
            'error_message': ''
        })

    @staticmethod
    def get():
        return_resp = {
            'feedback_id': '',
            'user_email': '',
            'type': '',
            'level': '',
            'message': '',
            'request_result': 'rejected',
            'error_message': ''
        }
        try:
            email = request.args.get('email')
            token = request.args.get('token')
            service_id = request.args.get('service_id')

            authorized = authorize(email, token, service_id)

            if authorized != '1':
                feedback_logger.info('User ' + str(email) + ' is not authorized.')
                return jsonify({'message': 'Unable to authorize this user', 'status': '0'})
        except Exception as ex:
            feedback_logger.exception(str(ex))
            feedback_logger.warning('credentials missing from the request.')
            return jsonify({"message": "Missing credentials.", "status": 0})

        try:
            feedback_id = request.args.get('feedback_id')
            if not feedback_id:
                feedback_logger.warning('feedback_id not present in the request.')
                return_resp['error_message'] = 'The feedback id cannot be empty!'
                return jsonify(return_resp)

        except Exception as ex:
            feedback_logger.exception('Error: '+str(ex))
            feedback_logger.exception('The user email or feedback id does not exist!')
            return_resp['error_message'] = 'The job id cannot be empty!'
            return jsonify(return_resp)

        try:
            feedback = Feedback.query.filter(Feedback.feedback_id == feedback_id,
                                             Feedback.user_email == email).first()

            # print(feedback.feedback_id)
            # print(dir(feedback))
            if feedback is None:
                return_resp['error_message'] = 'feedback_id is not found.'
            else:
                return_resp['request_result'] = 'finished'
                return_resp['feedback_id'] = feedback.feedback_id
                return_resp['user_email'] = feedback.user_email
                return_resp['type'] = feedback._type
                return_resp['timestamp'] = feedback.timestamp
                return_resp['level'] = feedback.level
                return_resp['message'] = feedback.message

            return jsonify(return_resp)
        except Exception as ex:
            db.session.rollback()
            db.session.remove()
            feedback_logger.exception('Error: '+str(ex))
            feedback_logger.exception('The user email or feedback id does not exist!')
            return_resp['error_message'] = 'The job id cannot be empty!'
            return jsonify(return_resp)

    @staticmethod
    def generate_job_id(submission_date):
        job_id_str = str(submission_date)
        job_id = hashlib.sha1(job_id_str.encode()).hexdigest()
        return job_id


def __register_feedback_resource():
    api.add_resource(FeedbackResource, '/submitfeedback')
