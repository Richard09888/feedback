HOST = '0.0.0.0'
PORT = 5000


USER_HOST = 'https://user-test.blocktest.info'
AUTHORIZE_URL = USER_HOST + '/blocktest/api/user/validate'


DB_USERNAME = 'prateek'  # 'root'
DB_PASSWORD = ''  # '123'
DB_ENDPOINT = '127.0.0.1:3306'

# test Unittest DB config
UNITTEST_DB_NAME = 'bdaas'
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://' + DB_USERNAME + ':' + DB_PASSWORD + '@' + DB_ENDPOINT + '/' + UNITTEST_DB_NAME

SQLALCHEMY_ECHO = False
SQLALCHEMY_TRACK_MODIFICATIONS = False
DEBUG = True
