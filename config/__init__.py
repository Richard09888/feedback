from flask import Flask
from flask_cors import CORS
application = Flask(__name__)
CORS(application)

# development config
# application.config.from_object('config.config_default')  # this is for local testing

# development config
application.config.from_object('config.config_development')  # this is for local testing

# online test config
# application.config.from_object('config.config_online_test')  # this is for online-testing environment



"""
Create the global db connection
"""
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy(application)
application.config['SQLALCHEMY_POOL_RECYCLE'] = 3600

"""
Create the global resources registration
"""
from flask_restful import Api
api = Api(application)

"""
Initialize the global logging system
"""
import logging


def __init_log():
    # set up logging to file - see previous section for more details
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] %(name)-12s {%(filename)s:%(lineno)d} %(levelname)-8s %(message)s',
                        datefmt='%m-%d %H:%M',
                        filename='access_log.log',
                        filemode='w')
    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('[%(asctime)s] %(name)-12s: {%(filename)s:%(lineno)d} %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)


__init_log()

"""
Initialize the loggers for each resources
"""
feedback_logger = logging.getLogger('resources.unittest_resource')







# initialize db
def __init_db():
    from model.model import __init_feedback_db
    __init_feedback_db()


__init_db()


# initialize resources
def __init_resource():
    from resources.feedback_resource import __register_feedback_resource


    __register_feedback_resource()



__init_resource()



