import requests
import json

from config import application


def authorize(email, token, service_id):
    url = application.config['AUTHORIZE_URL'] + '?email=' + str(email) + '&token=' + str(token) + '&service_id=' + str(service_id)
    req = requests.get(url)
    return json.loads(req.content)['status']